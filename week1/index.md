# **Website documentation**


This is actually my first time making a webpage, it was not easy one where I have to document all the steps I have to go through. but with the help with Our main instructor [Hashim Alsakkaf][flag_hashim] , without any hesitation he would offer to give us a hand to solve any problem we might face.

## Start with GIT
We were introduced to the version control system, So with the help [Hashim Alsakkaf][flag_hashim] in the prefabacademy he introduced us to [GitLab](https://about.gitlab.com/)

so we started by creating a new account in GitLab.

## Configuring Git

We started by launching the git bash terminal and to configure git by the following steps:

1. Entering  Gitlab username!

![](picture_git1.PNG)

2. Enter your Gitlab Email!

![](picture_git2.PNG)

this just to delete later
## Generating the “ssh” key

To do so, all you have to is to generate the SSH key you can follow the [Link](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
afterwards I copied the generated key my account in GitLab

## Creating a Website

 I used the open source template thanks to [ Bootstrap portfolio theme for freelancers](https://startbootstrap.com/themes/freelancer/),  
to edit the template to match  what I want, [learning (HTML) language is a must!](https://www.w3schools.com/html/ ) where it was used to edit my own page though Brackets software text editor where all my pre & fabacademy assigment will be documented,  

![](bracket.PNG)

### Markdown

Since there will be a lot of documentation where we have to write all the steps that we will go through  with rich  text  content it was much easier to use [markdown](https://en.wikipedia.org/wiki/Markdown)  markup language.
I used [Atom](https://atom.io/) software to write all the weekly content which integrates the markdown nicely.

but first I needed to learn the basics of the Markdown language, to start with markdown I referred to the  following: [start with Markdown](https://www.markdowntutorial.com/)

![](markdown.PNG)

##### converting Mardown to HTML

And since I'm documenting in markdown I needed to convert to HTML Language, so that I can link it to my main page, to do so I had to save the file in _md_ format then  I used the command line [Pandoc](https://pandoc.org/getting-started.html) which made it easy to convert the the md file to HTML, all what i have to do is to  write the following command in the git bash terminal where the _md_ file located

_pandoc index.md -f markdown -t html -s -o index.html_

#### Pushing the documentation in the archive
once I write down my documentation I needed at the end of the day to push to my gitlab account this is done by opening the terminal to then writing the following commands:

_git add .._  
_git commit- m "week"_  
_git push_  


####  Start with Css
I was not happy with the appearance of my weekly assignment, this why I needed to use the CSS template to style my documentation, so I used mycss.css styling which made my documentation more easy to read, at this stage my workflow is:

_pandoc index.md -f markdown -t html -c mycss.css -s -o index.html_  
_git add .._  
_git commit -m "message"_  
_git push_







[flag_hashim]: (http://archive.fabacademy.org/fabacademy2017/fablabuae/students/154/)
