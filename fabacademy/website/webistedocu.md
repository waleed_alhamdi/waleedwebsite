# **Website documentation**


This is actually my first time making a webpage, it was not easy one where I have to document all the steps I have to go through. but with the help with Our main instructor [Hashim Alsakkaf][flag_hashim] , without any hesitation he would offer to give us a hand to solve any problem we might face.

## Start with GIT
We were introduced to the version control system, So with the help [Hashim Alsakkaf][flag_hashim] in the prefabacademy he introduced us to [GitLab](https://about.gitlab.com/)

so we started by creating a new account in GitLab.

## Configuring Git

We started by launching the git bash terminal and to configure git by the following steps:

1. Entering  Gitlab username!

![alt text](Picture_git1.png)

2. Enter your Gitlab Email!

![alt text](Picture_git2.png)

## Generating the “ssh” key

To do so, all you have to is to generate the SSH key you can follow the [Link](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
afterwards I copied the generated key my account in GitLab




[flag_hashim]: (http://archive.fabacademy.org/fabacademy2017/fablabuae/students/154/)
