# **Electronic Production**

So, the aim of this week was to expose ourself to the monofab Srm-20 where we have to mill and fabricate our our ISP programmer board circuit which will be used later to program my electronics boards.

### Downloading ISP Traces
We were advises by [Hashim Alsakkaf][flag_hashim] to [brian's](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html
) documentation to fabricate our own ISP programmer.

to do so

First, I download the PNG file for traces.  

![](pic/fts_mini_traces.PNG)  

then, downloaded the PNG file for the outline  

![](pic/fts.PNG)

## Fab Modules
I used the [fab modules](http://fabmodules.org/), to convert the PNG traces to rml files, to do so all what you have to do is the following:
1. Upload the traces PNG file to the Fab modules website.
2. Choose the output format, which is in my case is roland mill (rml format).
3.Choose the process, for the inner traces it is (1/64) and for the outline I used the (1/16).
4. Then the final setting where you need to select the machine type and make sure that the home position is at , and press calculate.

![](pic/traces.PNG)

## Setting Up the srm_20

It was a bit tricky to get access to the machine physically, the closed frame of the machine made it difficult to change the mill bit,
to start cutting the traces we needed to replace the mill bit with 1/64 mill bit, to so i use L key to replace the mill bit,
once the mill bit is replaced, I set the x and y axis to the required home position

![](pic/srmsetup1.jpeg)

 then i started setting up the home position of the z axis, this was tricky one where you have to release the mill bit while holding it so it does not get broken then carefully bring the mill bit down all the way and make sure it hit the copper board,
once I was satisfied with home z axis position I uploaded the traces rml file and start cutting the file, once the traces process was over, I replaced the mill bit with 1/16 for the outline cut and I set the z home position repeated the same procedure as before, but this time I made sure not to change the x and y home position.

once I uploaded the outline rml file, and the cut  process was over I was not really happy about the result as you can see from the pic.

![](pic/isbboard1.jpeg)

After consulting our main instructor [Hashim Alsakkaf][flag_hashim] he pointed out that the z axis home position set up was the problem, to be honest i was not sure about it, Anyway, I repeated the process again but this time I replaced the 1/32 mill bit where I had some doubt that it might be the problem along with double checking z axis home position this is by pressing the 1/64 mill bit firmly towards the copper board.

THIS time I was really happy about the result.

![](pic/isbboard2.jpeg)


Soldering the components of the ISP board

I collected and listed all the components of the ISP board

![](pic/components1.jpeg)

even though it was not the first time soldering the smd components  I faced some trouble dealing with componentd, where it  might slip away even though I was using the tweezer , nevertheless I could manage soldering all the components paying attention the  polarity and orientation to some parts such as the Led where it has a small green line to indicate the negative polarity, also the diode and the ATTINY.

![](pic/ispboard.jpeg)

## Programming my ISP

#### Setting up the laptop
So to start with programming my own ISP, I had to install different softwares to set up the development platform which gave me a headache!

So this was done by following [Brian's tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html) with alot of patience where you have to fulfill the following steps:  
1- Install the Atmel GNU Toolchain  
2- Install GNU Make  
3-Install avrdude  
4-Update your PATH  
**WARNING!**      at this point I faced a problem at this stage which took an hour to solve where the paths in Brians tutorial does not match mine so I needed to update it accordingly   

![](pic/paths.PNG)

5-Install Drivers for your Programmer

#### Program the Attiny 44

I needed to use Hashim Alsakkaf ISP programmer taking in mind all the instructions that was pointed out by [Brian](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html), then I connected the programmer to the ISP header in my board,

![](pic/ispprog.jpeg)

 followed by running the following commands to program my board:

  _make flash_   to erase my chip, and program it with the _.hex_ file that was built before.  
  _make fuses_    to set up all of the fuses except the one that disables the reset pin  
_make rstdisbl_   to set up the fuse that disables the reset pin






























[flag_hashim]: (http://archive.fabacademy.org/fabacademy2017/fablabuae/students/154/)
